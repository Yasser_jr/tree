﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQLite;
using System.IO;

namespace Avis_Patients.Droid
{
   public class SqliteDB
    {
        public string dbpath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "Avis2.db3");
        

        [Table("Avis")]
        public class Avis
        {
            public string Q1 { get; set; }
            public string Q2 { get; set; }
            public string Q3 { get; set; }
            public string Q4 { get; set; }
            public string Q5 { get; set; }
            

        }
       

        public SqliteDB()
        {
            if (!File.Exists(dbpath))
            {
                var db = new SQLiteConnection(dbpath);
                db.CreateTable<Avis>();
                db.CreateTable<Login>();
            }
        }
            public void insert(string Q1, string Q2, string Q3, string Q4, string Q5)
            {
            var db = new SQLiteConnection(dbpath);
            var newAvis = new Avis();
            newAvis.Q1 = Q1;
            newAvis.Q2 = Q2;
            newAvis.Q3 = Q3;
            newAvis.Q4 = Q4;
            newAvis.Q5 = Q5;
            
            db.Insert(newAvis);
             }
        public string selectall()
        {
            string data = "";
            var db = new SQLiteConnection(dbpath);
            Console.WriteLine("reading data from Table");
            var table = db.Table<Avis>();
            foreach (var s in table)
            {
                data +="Reponse1:  "+ s.Q1 + "    "+ "Reponse2: "+ s.Q2 + "    " + "Reponse3: " + s.Q3 + "    " + "Reponse4: " + s.Q4 + "    " + "Reponse5: " + s.Q5 + "%" +"\n";
            }
            return data;

        }
        //******************************************************************************************************************************
        [Table("Login")]
        public class Login
        {
            public string login { get; set; }
            public string motdepass { get; set; }



        }
        public void insert1(string login, string motdepass)
        {
           
            var db = new SQLiteConnection(dbpath);
            var newlogin = new Login();
            newlogin.motdepass = motdepass;
            newlogin.login = login;
            db.Insert(newlogin);

        }
        public string selectlogin()
        {

            var db = new SQLiteConnection(dbpath);
            string login = "";
        
            var table = db.Table<Login>();
            foreach (var s in table)
            {
                login = s.login;
               
            }
            return login;

        }
        public string selectmotdepps()
        {
            var db = new SQLiteConnection(dbpath);
            
            string motdepss = "";
            var table = db.Table<Login>();
            foreach (var s in table)
            {
                
                motdepss = s.motdepass;
            }
            return motdepss;

        }
        }
}